import express, { Express } from 'express';
import { bruteforce } from '../../security/ExpressBrute';
import { authenticate, ChangePass, register } from './controller';
const Router = express.Router();

Router.post(`/register`, bruteforce.prevent, register);
Router.post(`/authenticate`, bruteforce.prevent, authenticate);
Router.post(`/changePass`, bruteforce.prevent, ChangePass);

export default Router;