import { Request, Response, NextFunction } from 'express';
import bcrypt from 'bcryptjs';
import * as Yup from 'yup';
import { model_user } from '../../models/User/model';
import { generateToken } from '../../helpers/auth';
import { authConfig } from '../../config/auth';

export const register = async (req:Request, res:Response, next:NextFunction) => {
    // #swagger.tags = ['User']
    /*  #swagger.parameters['obj'] = { 
        in: 'body',
        description: "Add a user",
        schema: { $ref: "#/definitions/User" }
    } */
    const { email, password } = req.body
    try {
        const userExists = await model_user.findOne({email: email});

        if (userExists) throw new Error('User already exists');

        const passwordHash = bcrypt.hashSync(password, 10);

        const user = await model_user.create({...req.body, password: passwordHash});
        
        return res.status(201).json({ token: generateToken(user._id, authConfig.secret, 86400) });


    } catch ( error ) {
        return res.status(400).send({ error: 'registration failed', consoleError: error.message });
    }
};

export const authenticate =  async (req:Request, res:Response, next:NextFunction) => {
    // #swagger.tags = ['User']
    /*  #swagger.parameters['obj'] = { 
        in: 'body',
        description: "Auth a user",
        schema: { "email": "example@mail.com", "password": "password"}
    } */

    const { email, password } = req.body;
    try {

        const schema = Yup.object().shape({
            email: Yup.string().email().required(),
            password: Yup.string().required(),
        });

        if (!(await schema.validate(req.body) )) throw new Error('Validation fails');

        const user = await model_user.findOne({email: email});
        if (!user) throw new Error('User not found');
        const compare = await bcrypt.compare(password, user.password);
        if (!compare) throw new Error('Invalid password');
            /* #swagger.responses[200] = { 
                    description: 'User successfully auth.',
                    schema: {"token": "any_token"}
            } */
        return res.status(200).send({token: generateToken(user._id, authConfig.secret, 1800)})

    } catch (error) {
        return res.status(400).send({ error: 'authenticate failed', consoleError: error.message });
    };
};

export const ChangePass =  async (req:Request, res:Response, next:NextFunction) => {
    // #swagger.tags = ['User']
    // #swagger.description = 'Adicionar válidação no front-end para usuário confirmar nova senha'
    /*  #swagger.parameters['obj'] = { 
        in: 'body',
        description: "Change user pass",
        schema: { "email": "example@mail.com", "password": "password", "newPassword": "newPassword"}
    } */
    const { email, password, newPassword } = req.body;
    try {
        const user = await model_user.findOne({email: email});
        if (!user) throw new Error('User not found');
        const compare = await bcrypt.compare(password, user.password);
        if (!compare) throw new Error('Invalid password');
        const passwordHash = bcrypt.hashSync(newPassword, 10);
        user.password = passwordHash;
        await user.save();

        return res.status(202).send({ message: 'Your password as changed' });

    } catch (error) {
        return res.status(400).send({ error: 'change pass failed', consoleError: error.message });
    };
};