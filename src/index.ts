import app, { port } from "./server/server";


app.listen(port, () => {
    console.log(`App is running on port ${port}`);
});