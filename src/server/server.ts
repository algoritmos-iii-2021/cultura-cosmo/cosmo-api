import Express, { NextFunction, Request, Response } from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import router from '../router/router';
import swaggerUi from 'swagger-ui-express';
import Helmet from 'helmet';
const swaggerFile = require('../../swagger_output.json');
export const port = process.env.PORT || 4000;
const app = Express();

app.use(cors());

app.use('/public', Express.static(__dirname+'/public')); // GET http://localhost:4000/public

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(Helmet());

app.get('/', (req, res) => {// GET http://localhost:4000 OU http://localhost:4000/
   res.send({message: `App is running on port ${port}`});
});

app.use('/api', router); // http://localhost:4000/api

app.use('/api-docs', function(req: any, res: Response, next: NextFunction){
  swaggerFile.host = req.get('host');
  req.swaggerDoc = swaggerFile;
  next();
}, swaggerUi.serve, swaggerUi.setup(swaggerFile, {customSiteTitle: 'Marea API', customCssUrl: '/public/index.css', customJs: '/public/index.js', customfavIcon: '/public/Logo_Marea.png'}));
export default app;