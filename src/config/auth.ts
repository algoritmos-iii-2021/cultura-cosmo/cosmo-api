export const authConfig = {
    secret: "6e1bd4dec2c79f111cdd51412b02f2fa",
    public: `-----BEGIN PUBLIC KEY-----
    MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzyu+c+Mqb3wbo3xj9rvC
    A5fZgy6qonb9fPcQccBSeKITtgzdV4A+/cv1FI3gJk65WZwgo5ktvCMJ9c5pcuXr
    fhaSbjBEdu9k5TX90/WP4S85eyt2rZ215iQysBkkf0vwYjqntnXrTY0PypJNi9ld
    mrep4jrgLAHU+zNXumMYG1fhw/qGpdL/j842kfZZJLbmhq8o9LZfEd6ND1fIPq7i
    Hhe8keTnahJ2wQC8a4ghNFlEZ/3rvyxvyFFAFJyDc1tHYC3WwtuHnrBRFJmnFDrZ
    UJnnFqa6gLW41ZtnhoiPI8NkhbEY37ypnRJ7zsI5eW6X5TJacvhAaI/u4jiKw9wM
    QQIDAQAB
    -----END PUBLIC KEY-----`,
    private: `-----BEGIN RSA PRIVATE KEY-----
    MIIEpAIBAAKCAQEAzyu+c+Mqb3wbo3xj9rvCA5fZgy6qonb9fPcQccBSeKITtgzd
    V4A+/cv1FI3gJk65WZwgo5ktvCMJ9c5pcuXrfhaSbjBEdu9k5TX90/WP4S85eyt2
    rZ215iQysBkkf0vwYjqntnXrTY0PypJNi9ldmrep4jrgLAHU+zNXumMYG1fhw/qG
    pdL/j842kfZZJLbmhq8o9LZfEd6ND1fIPq7iHhe8keTnahJ2wQC8a4ghNFlEZ/3r
    vyxvyFFAFJyDc1tHYC3WwtuHnrBRFJmnFDrZUJnnFqa6gLW41ZtnhoiPI8NkhbEY
    37ypnRJ7zsI5eW6X5TJacvhAaI/u4jiKw9wMQQIDAQABAoIBAEbx+P+gnIifHKvo
    zibRd4hZeQBme8K1MuuXhAcgUF3FSzC9yRLM9UVekigeydL0hJ6+S6MassHJ0gBe
    VlglinacbjrTxq5gHtIroify4PnBfVLDuJmN95u/a6hIg+of7EsHQSXHzfO5yd2a
    KnoozthL6x5xARGw8umvVGycwMWjvdL+Lti7x0cjxHsOYhnsWh2pcQfPRpJqCcmm
    5kSSRw6oHmCKoD0Eaf2ctXUu1GYiGNOXigvhy06/2+0/W8nZQkadJnS6OZUqBLPq
    V6W1VewwljY9NcpR8RhQ3LSKlsTshUkiyq0TszFoxv4Rm0PNim+D5xHJ4EwZt3oi
    bViI4AECgYEA8ymzZZpSFOyG49rLx/PnXNGJbWV6rncnKjIsl3YxHLIL7l16LnQW
    ZAjdQnyACZYlDwP9whK+NaEW+batjy4cSb0HcRifoDbPj64BVOe4YE296sZZBYYA
    YCZYDUQ7CxTgs/70SqMNmnwiX9fQMuvcmZLIWoPMlB/dGd2Kjyd53MECgYEA2hue
    OIsr3kYeqaLo2fwMtiLuJsSZ/Adheu+At/4VxPSDePdvz/jiy7w5wLOzxHmjz9oZ
    5n6uIvf6sqBfwR7fm1MawAexSVbLmPzX3HTW7pLitgFDCpd/BeozrffNbUAXMSy6
    AHB3QSsixeoYO/FnWkJTqKdmgfQ+TmvUzkIkj4ECgYEAhZnb328yuhxa4MZOQMZZ
    0D2wbFLdXQ5KjnyS04U05rdMpuLlRrCxvLTCHM2a66l7IwzmV8NEjuK7ZOhh49FJ
    6ZnahOEqkzSKRj8srCHEw+Ty/BOOxeIiQrHOnztl1WjbLQpf3sJHryGVIWPKGLBO
    vHJRcqoHtTrsmFGI166w3gECgYEAgQKnExEXELHvPy4Sh6X0KuP8dEgEQTRob5wt
    Sifr3oU3Ix1RMPYpyJL8okRpSAUfZTm/6jvyatK9ol0EZqoJX8nHX+Ds99F0OnkM
    geVGoPWXTkruydwUkX4XtYaHJ2Eqlra7Crl7gLuwJNs0HHjNRnFHJNJn6uR62k4k
    ISC0mYECgYBDtnIroDBGJ/TvwgCvCXuN9OeDLUS+7uk5HU3NE/1UU5I3sYunZBT0
    hocAJkZkqIYhJChb2V+WR0lQgBAWIWMxlhGzyoAQMsIxJatG4/4VdcbBDb1xEQOY
    xBq8kBG4UWjS9jbqhLK1ZeBRAQakVE1bjwPTDmPKJXMTwAc/XNOjoQ==
    -----END RSA PRIVATE KEY-----`
}