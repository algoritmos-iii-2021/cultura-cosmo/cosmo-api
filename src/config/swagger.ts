const swaggerAutogen = require('swagger-autogen')({
    language: 'pt-BR'
})

const doc = {
    info: {
        version: "0.1.0",
        title: "Marea API",
        description: "Documentation for Marea devs.",
    },
    basePath: "/",
    schemes: ['http'],
    consumes: ['application/json'],
    produces: ['application/json'],
    tags: [
        {
            "name": "User",
            "description": "API for Users Endpoints",
        },
    ],
    definitions: {
        User: {
            active: true,
            $email: "example@mail.com",
            $doc: "000.000.000-11",
            $username: "jhon doe",
            $password: "password",
            name: "Jhon",
            lastname: "Doe",
            birthday: "20/02/2002",
            permissions: {
                superuser: false,
                admin: false,
            },
            streetLine1: "Rua imaginaria",
            streetLine2: "Casa",
            num: "258",
            city: "Pelotas",
            stateCode: "RS",
            zipCode: "00000000",
        },
    }
}

const outputFile = './swagger_output.json'
const endpointsFiles = ['src/server/server.ts']

swaggerAutogen(outputFile, endpointsFiles, doc).then(() => {
    require('../index')
});