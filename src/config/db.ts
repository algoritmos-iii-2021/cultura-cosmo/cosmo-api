import mongoose from 'mongoose';

mongoose.connect('mongodb+srv://cosmo-dev:cosmo-dev@cluster0.1m3b9.mongodb.net/marea-dev?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error: '))

export default mongoose;

export abstract class DefaultSchema extends mongoose.Schema {
    constructor(definition?: {} | undefined, options?: mongoose.SchemaOptions | undefined) {
        definition = {
        created_at: {
            type: Date,
            default: Date.now,
        },
        updated_at: {
            type: Date,
            default: Date.now,
        },
        deleted_at: {
            type: Date,
            default: null,
        },
        ...definition,
        }
        super(definition, options);
    }
    public abstract secret(secretKey: string): boolean;
}