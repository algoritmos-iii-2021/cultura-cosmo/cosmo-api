import express, { Express } from 'express';
import userRoutes from '../models/User/router';
const Router = express.Router();

Router.use('/user', userRoutes);

export default Router;
