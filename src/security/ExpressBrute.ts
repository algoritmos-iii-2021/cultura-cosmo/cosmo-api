import ExpressBrute from "express-brute";
import mongoose from "../config/db";
const MongooseStore = require("express-brute-mongoose");
const BruteForceSchema = require("express-brute-mongoose/dist/schema");

const model = mongoose.model(
  "bruteforce",
  new mongoose.Schema(BruteForceSchema)
);
const store = new MongooseStore(model);

export const bruteforce = new ExpressBrute(store, {
    freeRetries: 2000,
    minWait: 1 * 60 * 1000,
    maxWait: 60 * 60 * 1000,
    lifetime: 7 * 24 * 60 * 60,
});